﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Threading;

namespace deflacWpf
{
    public partial class MainWindow
    {
        private readonly ConcurrentQueue<string> _fileList = new ConcurrentQueue<string>();
        private readonly ObservableCollection<Item> _flacList = new ObservableCollection<Item>();
        private readonly ObservableCollection<Item> _messageList = new ObservableCollection<Item>();
        private string _outputDir;

        public ObservableCollection<Item> FlacList{get { return _flacList; }}
        public ObservableCollection<Item> MessageList{ get { return _messageList; }}

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void CheckForFlac(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                string[] folder = Directory.GetDirectories(path);
                string[] outerFiles = Directory.GetFiles(path);

                foreach (string item in outerFiles)
                {
                    if (item.EndsWith(".flac"))
                    {
                        _flacList.Add(new Item(item));
                        _fileList.Enqueue(item);
                    }
                }

                foreach (string folderName in folder)
                {
                    string[] innerFiles = Directory.GetFiles(folderName);

                    foreach (string item in innerFiles)
                    {
                        if (item.EndsWith(".flac"))
                        {
                            _flacList.Add(new Item(item));
                            _fileList.Enqueue(item);
                        }
                    }
                }
                progressBar.Maximum = _fileList.Count();
            }
        }

        private void Action()
        {
            string item;
            while (_fileList.TryDequeue(out item))
            {
                flacToWav(item, item.Replace("flac", "wav"));

                string[] split = item.Split('\\');
                var output = new FileInfo(_outputDir + split[split.Length - 2] + "\\" + split[split.Length - 1].Replace(".flac", ".mp3"));

                if (output.DirectoryName != null && !Directory.Exists(output.DirectoryName))
                    Directory.CreateDirectory(output.DirectoryName);

                WavToMp3(item.Replace("flac", "wav"), output.FullName);
                Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(delegate { progressBar.Value = progressBar.Value + 1; }));

                _flacList.Remove(new Item(item));
            }
        }

        private void WavToMp3(string input, string output)
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = "lame.exe",
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            if (File.Exists(input))
            {

                var inputInfo = new FileInfo(input);
                var outputInfo = new FileInfo(output);

                var back = new StringBuilder();
                back.Append("-V2 --vbr-new ");
                back.Append("\"");
                back.Append(inputInfo.FullName);
                back.Append("\"");
                back.Append(" ");
                back.Append("\"");
                back.Append(outputInfo.FullName);
                back.Append("\"");

                startInfo.Arguments = back.ToString();

                try
                {
                    Process.Start(startInfo);

                    messageList_Add("Finished " + outputInfo.Name);
                }
                catch(Exception e)
                {
                    System.Windows.MessageBox.Show("Error: " + e.Message);

                    messageList_Add("Error " + outputInfo.FullName);
                }
            }
            else
            {
                Console.WriteLine(@"Cant find files");
            }
        }

        private void messageList_Add(string message)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() => _messageList.Add(new Item(message))));
        }

        private void flacToWav(string input, string output)
        {
            const bool checkMd5 = true;

            IntPtr decoder = IntPtr.Zero;
            BinaryWriter bw = null;

            try
            {
                bw = new BinaryWriter(new FileStream(output, FileMode.Create, FileAccess.Write, FileShare.Read));
            }
            catch (Exception e)
            {
                Console.WriteLine(@"ERROR: opening [{0}] for output {1}", output, e.Message);
            }
            try
            {
                if (IntPtr.Zero == (decoder = flac.FLAC__stream_decoder_new()))
                {
                    Console.WriteLine(@"ERROR: allocating decoder");
                    if (bw != null) bw.Close();
                }
                flac.FLAC__stream_decoder_set_md5_checking(decoder, checkMd5);
                flac.d_write_callback dWc = flac.write_callback;
                flac.d_metadata_callback dMc = flac.metadata_callback;
                flac.d_error_callback dEc = flac.error_callback;
                //
                flac.FLAC__StreamDecoderInitStatus initStatus = flac.FLAC__stream_decoder_init_file(decoder, input, dWc, dMc, dEc, /*client_data=*/bw);
                if (initStatus != flac.FLAC__StreamDecoderInitStatus.FLAC__STREAM_DECODER_INIT_STATUS_OK)
                {
                    throw new Exception(string.Format("ERROR: initializing decoder: {0}", initStatus));
                }

                if (flac.FLAC__stream_decoder_process_until_end_of_stream(decoder))
                {
                    Console.WriteLine(@"Decoding succeeded");
                }
                else
                {
                    throw new Exception("ERROR: decoding FAILED");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(@"ERROR: {0}", e.Message);
                //
                if (null != bw) bw.Close();
                if (File.Exists(output)) File.Delete(output);
            }
            finally
            {
                if (IntPtr.Zero != decoder) flac.FLAC__stream_decoder_delete(decoder);
                if (null != bw) bw.Close();
            }
        }

        #region Events

        void threadSpinner_DoWork(object sender, DoWorkEventArgs e)
        {
            var listActions = new List<Action>();
            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                listActions.Add(Action);
            }
            Parallel.Invoke(listActions.ToArray());
        }

        private void browse_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderBrowserDialog();

            dlg.ShowDialog();

            CheckForFlac(dlg.SelectedPath);
        }

        private void browseOut_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderBrowserDialog();

            dlg.ShowDialog();

            if (dlg.SelectedPath.EndsWith("\\"))
            {
                _outputDir = dlg.SelectedPath;
                lblOutputDir.Content = dlg.SelectedPath;
            }
            else
            {
                _outputDir = dlg.SelectedPath + "\\";
                lblOutputDir.Content = dlg.SelectedPath + "\\";
            }
        }

        private void convert_Click(object sender, RoutedEventArgs e)
        {
            _messageList.Clear();

            var threadSpinner = new BackgroundWorker();
            threadSpinner.DoWork += threadSpinner_DoWork;
            threadSpinner.RunWorkerAsync();
        }

        #endregion
    }
}
