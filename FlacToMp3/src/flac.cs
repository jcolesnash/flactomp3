﻿using System;
using System.Runtime.InteropServices;
using System.IO;

namespace deflacWpf
{
    class flac
    {
        public enum FLAC__StreamDecoderWriteStatus
        {
            FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE,
            FLAC__STREAM_DECODER_WRITE_STATUS_ABORT
        }; 

        const uint FLAC__MAX_CHANNELS = 8;
        const uint FLAC__MAX_FIXED_ORDER = 4;
        const uint FLAC__MAX_LPC_ORDER = 32;

        public enum FLAC__StreamDecoderInitStatus
        {
            FLAC__STREAM_DECODER_INIT_STATUS_OK = 0,
            FLAC__STREAM_DECODER_INIT_STATUS_UNSUPPORTED_CONTAINER,
            FLAC__STREAM_DECODER_INIT_STATUS_INVALID_CALLBACKS,
            FLAC__STREAM_DECODER_INIT_STATUS_MEMORY_ALLOCATION_ERROR,
            FLAC__STREAM_DECODER_INIT_STATUS_ERROR_OPENING_FILE,
            FLAC__STREAM_DECODER_INIT_STATUS_ALREADY_INITIALIZED
        }

        public enum FLAC__MetadataType
        {
            FLAC__METADATA_TYPE_STREAMINFO = 0,
            FLAC__METADATA_TYPE_PADDING = 1,
            FLAC__METADATA_TYPE_APPLICATION = 2,
            FLAC__METADATA_TYPE_SEEKTABLE = 3,
            FLAC__METADATA_TYPE_VORBIS_COMMENT = 4,
            FLAC__METADATA_TYPE_CUESHEET = 5,
            FLAC__METADATA_TYPE_PICTURE = 6,
            FLAC__METADATA_TYPE_UNDEFINED = 7
        }

        public enum FLAC__ChannelAssignment
        {
            FLAC__CHANNEL_ASSIGNMENT_INDEPENDENT = 0, 
            FLAC__CHANNEL_ASSIGNMENT_LEFT_SIDE = 1, 
            FLAC__CHANNEL_ASSIGNMENT_RIGHT_SIDE = 2, 
            FLAC__CHANNEL_ASSIGNMENT_MID_SIDE = 3
        }
        public enum FLAC__FrameNumberType
        {
            FLAC__FRAME_NUMBER_TYPE_FRAME_NUMBER, 
            FLAC__FRAME_NUMBER_TYPE_SAMPLE_NUMBER 
        }
        public enum FLAC__SubframeType
        {
            FLAC__SUBFRAME_TYPE_CONSTANT = 0,
            FLAC__SUBFRAME_TYPE_VERBATIM = 1,
            FLAC__SUBFRAME_TYPE_FIXED = 2, 
            FLAC__SUBFRAME_TYPE_LPC = 3 
        }
        public enum FLAC__EntropyCodingMethodType
        {
            FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE = 0,
            FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE2 = 1
        }

        public enum FLAC__StreamDecoderErrorStatus
        {
            FLAC__STREAM_DECODER_ERROR_STATUS_LOST_SYNC,
            FLAC__STREAM_DECODER_ERROR_STATUS_BAD_HEADER,
            FLAC__STREAM_DECODER_ERROR_STATUS_FRAME_CRC_MISMATCH,
            FLAC__STREAM_DECODER_ERROR_STATUS_UNPARSEABLE_STREAM
        }
        [StructLayout(LayoutKind.Explicit)]
        public struct FLAC__FrameHeader
        {
            [FieldOffset(0)]
            public uint blocksize;
            [FieldOffset(4)]
            public uint sample_rate;
            [FieldOffset(8)]
            public uint channels;
            [FieldOffset(12)]
            public FLAC__ChannelAssignment channel_assignment;
            [FieldOffset(16)]
            public uint bits_per_sample;
            [FieldOffset(20)]
            public FLAC__FrameNumberType number_type;
            [FieldOffset(24)]
            public UInt32 frame_number;
            [FieldOffset(24)]
            public UInt64 sample_number;
            [FieldOffset(28)]
            public byte crc;
        }
        public struct FLAC__Subframe_Constant
        {
            public Int32 value;
        }
        public struct FLAC__Subframe_Verbatim
        {
            public IntPtr data;
        }

        public struct FLAC__EntropyCodingMethod_PartitionedRiceContents
        {
            public IntPtr parameters;
            public IntPtr raw_bitst;
            public uint capacity_by_order;
        }

        public struct FLAC__EntropyCodingMethod_PartitionedRice
        {
            uint order;
            IntPtr contents;
        }

        public struct FLAC__EntropyCodingMethod
        {
            public FLAC__EntropyCodingMethodType type;
            public struct data
            {
                public FLAC__EntropyCodingMethod_PartitionedRice partitioned_rice;
            }
        }

        public struct FLAC__Subframe_Fixed
        {
            public FLAC__EntropyCodingMethod entropy_coding_method;
            public uint order;
            public Int32 warmup0;
            public Int32 warmup1;
            public Int32 warmup2;
            public Int32 warmup3;
            public IntPtr residual;
        }

        public struct FLAC__Subframe_LPC
        {
            public FLAC__EntropyCodingMethod entropy_coding_method;
            public uint order;
            public uint qlp_coeff_precision;
            public int quantization_level;
            public Int32 qlp_coeff00, qlp_coeff01, qlp_coeff02, qlp_coeff03, qlp_coeff04, qlp_coeff05, qlp_coeff06, qlp_coeff07, qlp_coeff08, qlp_coeff09,
            qlp_coeff10, qlp_coeff11, qlp_coeff12, qlp_coeff13, qlp_coeff14, qlp_coeff15, qlp_coeff16, qlp_coeff17, qlp_coeff18, qlp_coeff19,
            qlp_coeff20, qlp_coeff21, qlp_coeff22, qlp_coeff23, qlp_coeff24, qlp_coeff25, qlp_coeff26, qlp_coeff27, qlp_coeff28, qlp_coeff29,
            qlp_coeff30, qlp_coeff31;
            public Int32 warmup00, warmup01, warmup02, warmup03, warmup04, warmup05, warmup06, warmup07, warmup08, warmup09,
            warmup10, warmup11, warmup12, warmup13, warmup14, warmup15, warmup16, warmup17, warmup18, warmup19,
            warmup20, warmup21, warmup22, warmup23, warmup24, warmup25, warmup26, warmup27, warmup28, warmup29,
            warmup30, warmup31;
            public IntPtr residual;
        }


        public struct FLAC__Subframe
        {
            public FLAC__SubframeType type;
            [StructLayout(LayoutKind.Explicit)]
            public struct _data
            {
                [FieldOffset(0)]
                public FLAC__Subframe_Constant constant;
                [FieldOffset(0)]
                public FLAC__Subframe_Fixed fixed_;
                [FieldOffset(0)]
                public FLAC__Subframe_LPC lpc;
                [FieldOffset(0)]
                public FLAC__Subframe_Verbatim verbatim;
            }
            public _data data;
            public uint wasted_bits;
        }

        public struct FLAC__FrameFooter
        {
            public UInt16 crc;//FLAC__uint16 crc;
        }

        public struct FLAC__Frame
        {
            public FLAC__FrameHeader header;
            public FLAC__Subframe subframes0;
            public FLAC__Subframe subframes1;
            public FLAC__Subframe subframes2;
            public FLAC__Subframe subframes3;
            public FLAC__Subframe subframes4;
            public FLAC__Subframe subframes5;
            public FLAC__Subframe subframes6;
            public FLAC__Subframe subframes7;
            public FLAC__FrameFooter footer;
        }
        public struct FLAC__StreamDecoder
        {
            IntPtr protected_;
            IntPtr private_;
        }
        //
        public struct FLAC__StreamMetadata_StreamInfo
        {
            public uint min_blocksize, max_blocksize;
            public uint min_framesize, max_framesize;
            public uint sample_rate;
            public uint channels;
            public uint bits_per_sample;
            public UInt64 total_samples;
            public byte
            md5sum01, md5sum02, md5sum03, md5sum04, md5sum05, md5sum06, md5sum07, md5sum08, md5sum09, md5sum10, md5sum11, md5sum12, md5sum13, md5sum14, md5sum15;
        }

        public struct FLAC__StreamMetadata
        {
            public FLAC__MetadataType type;
            
            public bool is_last;

            public uint length;

            [StructLayout(LayoutKind.Explicit)]
            public struct _data
            {
                [FieldOffset(0)]
                public FLAC__StreamMetadata_StreamInfo stream_info;
            };
            public _data data;
        }

        [DllImport("libFLAC.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr FLAC__stream_decoder_new();

        [DllImport("libFLAC.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool FLAC__stream_decoder_set_md5_checking(IntPtr decoder, bool value);

        [DllImport("libFLAC.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern FLAC__StreamDecoderInitStatus FLAC__stream_decoder_init_file(IntPtr decoder, string file, MulticastDelegate write_callback, MulticastDelegate metadata_callback, MulticastDelegate error_callback, BinaryWriter client_data);
        [DllImport("libFLAC.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern string FLAC__StreamDecoderInitStatusString(int init_status);

        [DllImport("libFLAC.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool FLAC__stream_decoder_process_until_end_of_stream(IntPtr decoder);

        [DllImport("libFLAC.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void FLAC__stream_decoder_delete(IntPtr decoder);

        public static UInt64 total_samples = 0;
        public static uint sample_rate = 0;
        public static uint channels = 0;
        public static uint bps = 0;

        [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
        public delegate FLAC__StreamDecoderWriteStatus d_write_callback(IntPtr decoder, IntPtr frame, IntPtr buffer, BinaryWriter client_data);
        //
        [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
        public delegate void d_metadata_callback(IntPtr decoder, IntPtr metadata, IntPtr client_data);
        //
        [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
        public delegate void d_error_callback(IntPtr decoder, FLAC__StreamDecoderErrorStatus status, IntPtr client_data);

        public static FLAC__StreamDecoderWriteStatus write_callback(IntPtr decoder, IntPtr frame, [MarshalAs(UnmanagedType.LPArray)] IntPtr buffer, BinaryWriter bw)
        {
            UInt32 total_size = (UInt32)(total_samples * channels * (bps / 8));

            FLAC__Frame frm = new FLAC__Frame();
            frm = (FLAC__Frame)Marshal.PtrToStructure(frame, typeof(FLAC__Frame));

            if (total_samples == 0)
            {
                Console.WriteLine("ERROR: this example only works for FLAC files that have a total_samples count in STREAMINFO");
                return FLAC__StreamDecoderWriteStatus.FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
            }
            if (channels != 2 || bps != 16)
            {
                Console.WriteLine("ERROR: this example only supports 16bit stereo streams");
                return FLAC__StreamDecoderWriteStatus.FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
            }

            if (frm.header.sample_number == 0)
            {
                try
                {
                    bw.Write(new Byte[] { 0x52, 0x49, 0x46, 0x46 });
                    bw.Write((UInt32)(total_size + 36));
                    bw.Write(new Byte[] { 0x57, 0x41, 0x56, 0x45, 0x66, 0x6d, 0x74, 0x20 });
                    bw.Write((UInt32)(16));
                    bw.Write((UInt16)(1));
                    bw.Write((UInt16)(channels));
                    bw.Write((UInt32)(sample_rate));
                    bw.Write((UInt32)(sample_rate * channels * (bps / 8)));
                    bw.Write((UInt16)(channels * (bps / 8)));
                    bw.Write((UInt16)(bps));
                    bw.Write(new Byte[] { 0x64, 0x61, 0x74, 0x61 });
                    bw.Write((UInt32)(total_size));
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR: write error {0}", e.Message);
                    return FLAC__StreamDecoderWriteStatus.FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
                }
                //
            }
            //
            IntPtr pLeft = IntPtr.Zero;
            IntPtr pRight = IntPtr.Zero;
            UInt16 uLeft, uRight;
            try
            {
                pLeft = Marshal.ReadIntPtr(buffer, 0);
                pRight = Marshal.ReadIntPtr(buffer, 4);
                for (int i = 0; i < frm.header.blocksize; i++)
                {
                    uLeft = (UInt16)Marshal.ReadInt16(pLeft, i * 4);
                    uRight = (UInt16)Marshal.ReadInt16(pRight, i * 4);
                    bw.Write(uLeft); bw.Write(uRight);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: write error {0}", e.Message);
                return FLAC__StreamDecoderWriteStatus.FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
            }

            return FLAC__StreamDecoderWriteStatus.FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
        }

        public static void metadata_callback(IntPtr decoder, IntPtr metadata, IntPtr client_data)
        {
            FLAC__StreamMetadata sm = new FLAC__StreamMetadata();
            sm = (FLAC__StreamMetadata)Marshal.PtrToStructure(metadata, typeof(FLAC__StreamMetadata));
            if (sm.type == FLAC__MetadataType.FLAC__METADATA_TYPE_STREAMINFO)
            {
                total_samples = sm.data.stream_info.total_samples;
                sample_rate = sm.data.stream_info.sample_rate;
                channels = sm.data.stream_info.channels;
                bps = sm.data.stream_info.bits_per_sample;

                Console.WriteLine("Sample rate    : {0} Hz", sample_rate);
                Console.WriteLine("Channels       : {0}", channels);
                Console.WriteLine("Bits per sample: {0}", bps);
                Console.WriteLine("Total samples  : {0}", total_samples);
            }
        }
        public static void error_callback(IntPtr decoder, FLAC__StreamDecoderErrorStatus status, IntPtr client_data)
        {
            Console.WriteLine("ERROR: got error callback: {0}", status);
        }
    }
}
