using System;
using System.ComponentModel;

namespace deflacWpf
{
    public class Item : INotifyPropertyChanged
    {
        private string _name;

        public Item(string name)
        {
            _name = name;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public string Name
        {
            get { return this._name; }
            set
            {
                if (!string.Equals(this._name, value, StringComparison.Ordinal))
                {
                    this._name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}